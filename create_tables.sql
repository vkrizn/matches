CREATE SEQUENCE public.hibernate_sequence
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

CREATE TABLE match (
    id bigint NOT NULL PRIMARY KEY,
    json_id integer,
    away_result integer,
    home_result integer,
    away_team_id bigint NOT NULL,
    home_team_id bigint NOT NULL,
    winning_team_id bigint
);

CREATE TABLE event (
    id bigint NOT NULL PRIMARY KEY,
    json_id integer,
    minute integer NOT NULL,
    match_id bigint NOT NULL,
    player_id bigint,
    scorer_id bigint,
    team_id bigint,
    event_type_id bigint NOT NULL
);

CREATE TABLE event_type (
    id bigint NOT NULL PRIMARY KEY,
    name character varying(255),
    type character varying(255)
);

CREATE TABLE team (
    id bigint NOT NULL PRIMARY KEY,
    json_id integer,
    name character varying(255) NOT NULL
);

CREATE TABLE player (
    id bigint NOT NULL PRIMARY KEY,
    json_id integer,
    name character varying(255) NOT NULL,
    points integer,
    surname character varying(255),
    team_id bigint NOT NULL
);

ALTER TABLE match ADD CONSTRAINT uk_4h19xymcjxskd775rj06dp8dq UNIQUE (json_id);
ALTER TABLE event ADD CONSTRAINT uk_r466ha5ojfpk0ib8ng62fx85a UNIQUE (json_id);
ALTER TABLE event_type ADD CONSTRAINT uk_th4tw406pfapsyck46jkm0t4u UNIQUE (type);
ALTER TABLE team ADD CONSTRAINT uk_1morjguk4805gef4pnyyvlbxj UNIQUE (json_id);
ALTER TABLE player ADD CONSTRAINT uk_m8c3p44b28ymcx019w4h427h9 UNIQUE (json_id);

ALTER TABLE match ADD CONSTRAINT fk6ihefb9r7f0fcm0xuves72b2l FOREIGN KEY (home_team_id) REFERENCES team(id);
ALTER TABLE match ADD CONSTRAINT fksyjor2anx7bkbst7ebyw13jcs FOREIGN KEY (away_team_id) REFERENCES team(id);
ALTER TABLE match ADD CONSTRAINT fkltiqlvp096m5pxrb5gf7f84et FOREIGN KEY (winning_team_id) REFERENCES team(id);
ALTER TABLE event ADD CONSTRAINT fkgxoo7ftgbsrwr4i27wb9ylu1 FOREIGN KEY (event_type_id) REFERENCES event_type(id);
ALTER TABLE event ADD CONSTRAINT fkerrr32ixed637xgpawv4mfcra FOREIGN KEY (match_id) REFERENCES match(id);
ALTER TABLE event ADD CONSTRAINT fko7r78aa4ksoidjb2oh13jsgc5 FOREIGN KEY (team_id) REFERENCES team(id);
ALTER TABLE event ADD CONSTRAINT fk8r2k8bndh9smhwvpjiv7qjlu7 FOREIGN KEY (scorer_id) REFERENCES player(id);
ALTER TABLE event ADD CONSTRAINT fkacbsfyuturyd55axsi1p9t5ly FOREIGN KEY (player_id) REFERENCES player(id);
ALTER TABLE player ADD CONSTRAINT fkdvd6ljes11r44igawmpm1mc5s FOREIGN KEY (team_id) REFERENCES team(id);


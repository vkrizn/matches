package si.vkrizn.interview.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class EventJson extends AbstractIdJson {
    private String name;
    private String type;
    private int time;
    private TeamTypeJson team;
    private PlayerJson player;
    private PlayerJson scorer;
}

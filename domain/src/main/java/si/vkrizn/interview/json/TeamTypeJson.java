package si.vkrizn.interview.json;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;

public enum TeamTypeJson {
    home,
    away,
    @JsonEnumDefaultValue
    none
}

package si.vkrizn.interview.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AbstractIdJson {
    @JsonProperty("_id")
    private Integer id;
}

package si.vkrizn.interview.json;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;
import java.util.Optional;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class MatchJson extends AbstractIdJson {
    @JsonIgnore
    private int resultHome;
    @JsonIgnore
    private int resultAway;
    @JsonIgnore
    private TeamTypeJson winner;
    @JsonIgnore
    private TeamJson home;
    @JsonIgnore
    private TeamJson away;

    public void setResult(Map<String, Object> result) {
        resultHome = (int) result.get("home");
        resultAway = (int) result.get("away");
        winner = Optional.ofNullable((String) result.get("winner"))
                .filter(s -> !s.isEmpty())
                .map(TeamTypeJson::valueOf)
                .orElse(TeamTypeJson.none);
    }

    public void setTeams(Map<String, TeamJson> teams) {
        home = teams.get("home");
        away = teams.get("away");
    }
}

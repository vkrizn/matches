package si.vkrizn.interview;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Optional;

@Entity
@Getter
@Setter
public class Player extends JsonAbstractPersistable {
    @Basic(optional = false)
    private String name;

    @Basic
    private String surname;

    @ManyToOne(optional = false)
    private Team team;

    @Basic
    private Integer points;

    public void addPoints(int points) {
        setPoints(Optional.ofNullable(this.points).orElse(0) + points);
    }
}

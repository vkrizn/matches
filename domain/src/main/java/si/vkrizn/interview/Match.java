package si.vkrizn.interview;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
public class Match extends JsonAbstractPersistable {

    @ManyToOne(optional = false)
    @JoinColumn(name = "home_team_id")
    private Team home;

    @ManyToOne(optional = false)
    @JoinColumn(name = "away_team_id")
    private Team away;

    @ManyToOne
    @JoinColumn(name = "winning_team_id")
    private Team winningTeam;

    @Basic
    private Integer homeResult;

    @Basic
    private Integer awayResult;

    @OneToMany(mappedBy = "match")
    @OrderBy("minute")
    private List<Event> events = new ArrayList<>();
}

package si.vkrizn.interview;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Entity;

@Getter
@Setter
@Entity
public class Team extends JsonAbstractPersistable {
    @Basic(optional = false)
    private String name;
}

package si.vkrizn.interview;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Match event type
 * Entity instead of an Enum in case a new event type is needed (e.g. different json file imported on initialization)
 */
@Getter
@Setter
@Entity
public class EventType extends AbstractPersistable<Long> {
    @Basic
    @Column(unique = true)
    private String type;
    private String name;
}

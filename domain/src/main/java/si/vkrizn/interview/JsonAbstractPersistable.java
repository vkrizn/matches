package si.vkrizn.interview;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Getter
@Setter
public class JsonAbstractPersistable extends AbstractPersistable<Long> {
    @Basic
    @Column(unique = true, updatable = false)
    private Integer jsonId;
}

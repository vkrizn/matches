package si.vkrizn.interview;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Event that happened in a match
 */
@Getter
@Setter
@Entity
public class Event extends JsonAbstractPersistable {
    @ManyToOne(optional = false)
    @JoinColumn(name = "event_type_id", updatable = false)
    private EventType type;

    @ManyToOne(optional = false)
    @JoinColumn(name = "match_id")
    private Match match;

    private int minute;

    @ManyToOne
    private Team team;

    @ManyToOne
    private Player player;

    @ManyToOne
    private Player scorer;

    /**
     * Finds player in event - either player or scorer field
     * @return
     */
    public Optional<Player> player() {
        return Stream.of(player, scorer)
                .filter(Objects::nonNull)
                .findFirst();
    }
}

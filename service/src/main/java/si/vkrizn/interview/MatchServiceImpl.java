package si.vkrizn.interview;

import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import si.vkrizn.interview.json.MatchJson;
import si.vkrizn.interview.json.TeamTypeJson;
import si.vkrizn.interview.repo.MatchRepository;

import java.util.Optional;

@Service
public class MatchServiceImpl extends JsonObjectImportService<Match, MatchJson> {

    @Autowired
    private MatchRepository dao;

    @Autowired
    private TeamServiceImpl teamService;

    @Override
    protected MatchRepository dao() {
        return dao;
    }

    @Override
    public Match findByJson(MatchJson matchJson) {
        return dao.findOne(QMatch.match.jsonId.eq(matchJson.getId()));
    }

    @Override
    protected void beforeSave(Match match) {
        Preconditions.checkState(match.getHome() != null && match.getAway() != null, "Shouldn't there be two teams playing against each other?");
    }

    @Override
    public Match generateFromJson(MatchJson matchJson) {
        Match match = new Match();
        match.setJsonId(matchJson.getId());
        match.setHomeResult(matchJson.getResultHome());
        match.setAwayResult(matchJson.getResultAway());
        match.setHome(teamService.importFromJson(matchJson.getHome()));
        match.setAway(teamService.importFromJson(matchJson.getAway()));
        match.setWinningTeam(Optional.ofNullable(matchJson.getWinner())
                .map(tt -> tt == TeamTypeJson.home ? match.getHome() : (tt == TeamTypeJson.away ? match.getAway() : null))
                .orElse(null));
        return match;
    }
}

package si.vkrizn.interview;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import si.vkrizn.interview.json.EventJson;
import si.vkrizn.interview.repo.EventTypeRepository;

@Service
public class EventTypeServiceImpl extends JsonObjectImportService<EventType, EventJson> {

    @Autowired
    private EventTypeRepository dao;

    @Override
    protected EventTypeRepository dao() {
        return dao;
    }

    @Override
    protected void beforeSave(EventType eventType) {
        Preconditions.checkState(!Strings.isNullOrEmpty(eventType.getType()), "Event type must be known");
    }

    @Override
    public EventType findByJson(EventJson eventJson) {
        return dao.findOne(QEventType.eventType.type.eq(eventJson.getType().toUpperCase()));
    }

    @Override
    public EventType generateFromJson(EventJson eventJson) {
        EventType eventType = new EventType();
        eventType.setType(eventJson.getType().toUpperCase());
        eventType.setName(eventJson.getName());
        return eventType;
    }
}

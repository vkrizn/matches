package si.vkrizn.interview;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import si.vkrizn.interview.json.PlayerJson;
import si.vkrizn.interview.repo.PlayerRepository;

@Service
public class PlayerServiceImpl extends BaseJsonServiceImpl<Player, PlayerJson> {

    @Autowired
    private PlayerRepository dao;

    @Override
    protected CrudRepository<Player, Long> dao() {
        return dao;
    }

    @Override
    protected void beforeSave(Player player) {
        Preconditions.checkState(!Strings.isNullOrEmpty(player.getName()), "Name please...");
        Preconditions.checkState(player.getTeam() != null, "Solo player?");
    }

    @Override
    public Player findByJson(PlayerJson playerJson) {
        return dao.findOne(QPlayer.player.jsonId.eq(playerJson.getId()));
    }

    @Transactional
    public Player importFromJson(PlayerJson playerJson, Team team) {
        Player player = findByJson(playerJson);
        if (player == null) {
            player = new Player();
            player.setJsonId(playerJson.getId());
            String name = playerJson.getName();
            String[] split = name.split(", ");
            if (split.length == 2) {
                player.setName(split[1]);
                player.setSurname(split[0]);
            } else {
                // fallback when player name is in expected format
                player.setName(name);
            }
            player.setTeam(team);
            save(player);
        }
        return player;
    }
}

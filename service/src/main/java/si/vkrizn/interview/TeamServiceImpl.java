package si.vkrizn.interview;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import si.vkrizn.interview.json.TeamJson;
import si.vkrizn.interview.repo.TeamRepository;

@Service
public class TeamServiceImpl extends JsonObjectImportService<Team, TeamJson> {

    @Autowired
    private TeamRepository dao;

    @Override
    protected TeamRepository dao() {
        return dao;
    }

    @Override
    protected void beforeSave(Team team) {
        Preconditions.checkState(!Strings.isNullOrEmpty(team.getName()), "A team needs a name!");
    }

    @Override
    public Team findByJson(TeamJson teamJson) {
        return dao.findOne(QTeam.team.jsonId.eq(teamJson.getId()));
    }

    @Override
    public Team generateFromJson(TeamJson teamJson) {
        Team team = new Team();
        team.setJsonId(teamJson.getId());
        team.setName(teamJson.getName());
        return team;
    }
}

package si.vkrizn.interview.projections;

import org.springframework.beans.factory.annotation.Value;

public interface PlayerProjection {
    Integer getJsonId();

    @Value("#{target.name} #{target.surname}")
    String getName();

    Integer getPoints();
}

package si.vkrizn.interview.projections;

import java.util.List;

public interface MatchProjection {
    Integer getJsonId();

    TeamProjection getHome();

    TeamProjection getAway();

    TeamProjection getWinningTeam();

    Integer getHomeResult();

    Integer getAwayResult();

    List<EventProjection> getEvents();
}

package si.vkrizn.interview.projections;

public interface TeamProjection {
    Integer getJsonId();

    String getName();
}

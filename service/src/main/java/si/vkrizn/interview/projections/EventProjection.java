package si.vkrizn.interview.projections;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Value;

public interface EventProjection {

    Integer getJsonId();

    @JsonIgnore
    MatchProjection getMatch();

    @Value("#{target.type.type}")
    String getType();

    @Value("#{target.type.name}")
    String getName();

    int getMinute();

    TeamProjection getTeam();

    PlayerProjection getPlayer();

    PlayerProjection getScorer();
}

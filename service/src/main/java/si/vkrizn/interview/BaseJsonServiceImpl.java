package si.vkrizn.interview;

import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public abstract class BaseJsonServiceImpl<P extends AbstractPersistable<Long>, J> {

    protected abstract CrudRepository<P, Long> dao();

    public abstract P findByJson(J j);

    @Transactional
    public void save(P p) {
        beforeSave(p);
        if (p.isNew())
            beforeInsert(p);
        dao().save(p);
    }

    protected void beforeInsert(P p) {
    }

    protected void beforeSave(P p) {
    }
}

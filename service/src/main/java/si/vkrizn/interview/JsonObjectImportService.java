package si.vkrizn.interview;

import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.transaction.annotation.Transactional;

/**
 * Base service for all Json import services
 *
 * @param <P> type of entity
 * @param <J> json type
 */
public abstract class JsonObjectImportService<P extends AbstractPersistable<Long>, J> extends BaseJsonServiceImpl<P, J> {

    @Transactional
    public P importFromJson(J j) {
        P p = findByJson(j);
        if (p == null) {
            p = generateFromJson(j);
            save(p);
        }
        return p;
    }

    public abstract P generateFromJson(J j);
}

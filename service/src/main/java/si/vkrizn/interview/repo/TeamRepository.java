package si.vkrizn.interview.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;
import si.vkrizn.interview.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long>, QueryDslPredicateExecutor<Team> {
}

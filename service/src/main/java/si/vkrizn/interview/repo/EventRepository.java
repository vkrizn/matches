package si.vkrizn.interview.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import si.vkrizn.interview.Event;
import si.vkrizn.interview.projections.EventProjection;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
public interface EventRepository extends JpaRepository<Event, Long>, QueryDslPredicateExecutor<Event> {
    @Transactional(readOnly = true)
    List<EventProjection> findByMatchJsonIdAndTypeTypeIgnoreCase(int matchJsonId, String type);

    @Transactional(readOnly = true)
    List<EventProjection> findByMatchJsonIdAndMinuteLessThan(int matchJsonId, int minute);

    @Transactional(readOnly = true)
    List<EventProjection> findByMatchJsonIdAndPlayerJsonId(int matchJsonId, int playerJsonId);

    @Transactional(readOnly = true)
    List<EventProjection> findByMatchJsonIdAndScorerJsonId(int matchJsonId, int playerJsonId);

    @Transactional(readOnly = true)
    default List<EventProjection> joinFindByMatchJsonIdAndPlayerOrScorerJsonId(int matchJsonId, int playerJsonId) {
        return Stream.concat(findByMatchJsonIdAndPlayerJsonId(matchJsonId, playerJsonId).stream(), findByMatchJsonIdAndScorerJsonId(matchJsonId, playerJsonId).stream()).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Query("select e from Event e where e.match.jsonId = :matchId and e.scorer.team = e.match.winningTeam")
    List<EventProjection> findWinningScorersEvents(@Param("matchId") int matchId);
}

package si.vkrizn.interview.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import si.vkrizn.interview.Match;
import si.vkrizn.interview.projections.MatchProjection;

@Repository
public interface MatchRepository extends JpaRepository<Match, Long>, QueryDslPredicateExecutor<Match> {
    @Transactional(readOnly = true)
    MatchProjection findByJsonId(int jsonId);
}

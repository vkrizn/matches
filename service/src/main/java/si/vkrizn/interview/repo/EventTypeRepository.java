package si.vkrizn.interview.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;
import si.vkrizn.interview.EventType;

@Repository
public interface EventTypeRepository extends JpaRepository<EventType, Long>, QueryDslPredicateExecutor<EventType> {
}

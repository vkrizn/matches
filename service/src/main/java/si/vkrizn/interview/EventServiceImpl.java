package si.vkrizn.interview;

import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import si.vkrizn.interview.json.EventJson;
import si.vkrizn.interview.json.TeamTypeJson;
import si.vkrizn.interview.repo.EventRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class EventServiceImpl extends BaseJsonServiceImpl<Event, EventJson> {

    @Value("${goal.points}")
    private int goalPoints;
    @Value("${card.points}")
    private int cardPoints;

    @Autowired
    private EventRepository dao;

    @Autowired
    private PlayerServiceImpl playerService;

    @Autowired
    private EventTypeServiceImpl eventTypeService;

    @Override
    protected EventRepository dao() {
        return dao;
    }

    @Override
    protected void beforeSave(Event event) {
        Preconditions.checkState(event.getType() != null, "Event type is a must");
        Preconditions.checkState(event.getMatch() != null, "Where did the event happen if not at a match?");
    }

    @Override
    protected void beforeInsert(Event event) {
        Player player = event.player().orElse(null);
        if (player != null) {
            if (event.getType().getType().equals("CARD")) {
                player.addPoints(cardPoints);
            } else if (event.getType().getType().equals("GOAL")) {
                player.addPoints(goalPoints);
            }
        }
    }

    @Override
    public Event findByJson(EventJson eventJson) {
        return dao.findOne(QEvent.event.jsonId.eq(eventJson.getId()));
    }

    public Event importFromJson(EventJson eventJson, EventType eventType, Team team, Player player, Player scorer, Match match) {
        Event event = findByJson(eventJson);
        if (event == null) {
            event = new Event();
            event.setJsonId(eventJson.getId());
            event.setMinute(eventJson.getTime());
            event.setMatch(match);
            event.setType(eventType);
            event.setTeam(team);
            event.setPlayer(player);
            event.setScorer(scorer);
            save(event);
        }
        return event;
    }

    public void importFromJson(List<EventJson> events, Match match) {
        Map<Integer, Player> playerLookup = new HashMap<>();
        Map<String, EventType> eventTypeLookup = new HashMap<>();

        for (EventJson event: events) {
            // get team by team type (home or away)
            Team team = event.getTeam() == TeamTypeJson.home ? match.getHome() : event.getTeam() == TeamTypeJson.away ? match.getAway() : null;

            // import or get player od scorer
            Player player = null;
            Player scorer = null;
            if (team != null) {
                player = Optional.ofNullable(event.getPlayer()).filter(p -> p.getId() != null).map(p -> {
                    Player pl;
                    if ((pl = playerLookup.get(p.getId())) == null)
                        playerLookup.put(p.getId(), pl = playerService.importFromJson(p, team));
                    return pl;
                }).orElse(null);
                scorer = Optional.ofNullable(event.getScorer()).filter(p -> p.getId() != null).map(p -> {
                    Player pl;
                    if ((pl = playerLookup.get(p.getId())) == null)
                        playerLookup.put(p.getId(), pl = playerService.importFromJson(p, team));
                    return pl;
                }).orElse(null);
            }

            // import or get event type
            EventType eventType;
            String eventTypeString = event.getType().toUpperCase();
            if ((eventType = eventTypeLookup.get(eventTypeString)) == null)
                eventTypeLookup.put(eventTypeString, eventType = eventTypeService.importFromJson(event));

            // import event
            importFromJson(event, eventType, team, player, scorer, match);
        }
    }
}

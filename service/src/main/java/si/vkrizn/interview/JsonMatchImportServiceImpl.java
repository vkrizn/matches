package si.vkrizn.interview;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import si.vkrizn.interview.json.MatchInfoJson;

@Service
public class JsonMatchImportServiceImpl {

    @Autowired
    private MatchServiceImpl matchService;
    @Autowired
    private EventServiceImpl eventService;

    @Transactional
    public void importMatch(MatchInfoJson matchInfo) {
        Match match = matchService.importFromJson(matchInfo.getMatch());
        eventService.importFromJson(matchInfo.getEvents(), match);
    }
}

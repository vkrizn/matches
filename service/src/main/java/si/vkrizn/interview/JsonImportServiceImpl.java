package si.vkrizn.interview;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import si.vkrizn.interview.json.DocJson;
import si.vkrizn.interview.json.MatchTimelineJson;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class JsonImportServiceImpl {

    @Autowired
    private JsonMatchImportServiceImpl matchImportService;

    public boolean importJson(String filePath) {
        try {
            ObjectMapper jsonMapper = new ObjectMapper();
            jsonMapper.enable(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE);
            MatchTimelineJson json = jsonMapper.readValue(Files.readAllBytes(Paths.get(filePath)), MatchTimelineJson.class);
            json.getDoc().stream()
                    .map(DocJson::getData)
                    .forEach(matchImportService::importMatch);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}

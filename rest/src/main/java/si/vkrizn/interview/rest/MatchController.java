package si.vkrizn.interview.rest;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import si.vkrizn.interview.projections.EventProjection;
import si.vkrizn.interview.projections.MatchEventsProjection;
import si.vkrizn.interview.projections.MatchPlayerProjection;
import si.vkrizn.interview.projections.MatchProjection;
import si.vkrizn.interview.repo.EventRepository;
import si.vkrizn.interview.repo.MatchRepository;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;

@RestController
@RequestMapping("/match")
public class MatchController {

    @Autowired
    private MatchRepository matchDao;
    @Autowired
    private EventRepository eventDao;
    @Autowired
    private ModelMapper mapper;

    // /match/[ID] : vrne vse podatke ki jih hranimo o tekmi z danim ID-jem
    @GetMapping("/{id}")
    public ResponseEntity<MatchProjection> match(@PathVariable("id") int id) {
        return wrap(matchDao.findByJsonId(id));
    }

    // /match/[ID]/event/[EVENT_TYPE] : vrne vse evente z danim event type-om
    @GetMapping("/{id}/event/{event_type}")
    public ResponseEntity<MatchProjection> events(@PathVariable("id") int id, @PathVariable("event_type") String eventType) {
        List<EventProjection> events = eventDao.findByMatchJsonIdAndTypeTypeIgnoreCase(id, eventType);
        MatchProjection match = findMatch(events, id);
        MatchEventsProjection matchProjection = filteredEventsProjection(match, events, MatchEventsProjection::new);
        return wrap(matchProjection);
    }

    // /match/[ID]/event/tominute/[MINUTE]: vrne vse evente, od začetka tekme, do dane minute [MINUTE]
    @GetMapping("/{id}/event/tominute/{minute}")
    public ResponseEntity<MatchProjection> eventsTillMinute(@PathVariable("id") int id, @PathVariable("minute") int minute) {
        List<EventProjection> events = eventDao.findByMatchJsonIdAndMinuteLessThan(id, minute);
        MatchProjection match = findMatch(events, id);
        MatchEventsProjection matchProjection = filteredEventsProjection(match, events, MatchEventsProjection::new);
        return wrap(matchProjection);
    }

    // /match/[ID]/player/[PLAYER_ID]: vrne seznam vseh eventov ter skupno stevilo doseženih točk danega igralca [PLAYER_ID]
    @GetMapping("/{id}/player/{player_id}")
    public ResponseEntity<MatchProjection> eventsForPlayer(@PathVariable("id") int id, @PathVariable("player_id") int playerId) {
        List<EventProjection> events = eventDao.joinFindByMatchJsonIdAndPlayerOrScorerJsonId(id, playerId);
        MatchProjection match = findMatch(events, id);
        MatchEventsProjection matchProjection = filteredEventsProjection(match, events, MatchEventsProjection::new);
        return wrap(matchProjection);
    }

    // /match/[ID]/winnerscorers : vrne seznam igralcev, ki so dali gole za zmagovalno ekipo. če je rezultat tekme neodločen, vrne podatke o vseh igralcih (obeh ekip), ki so dali gole.
    @GetMapping("/{id}/winnerscorers")
    public ResponseEntity<MatchProjection> winnerScorers(@PathVariable("id") int id) {
        List<EventProjection> events = eventDao.findWinningScorersEvents(id);
        MatchProjection match = findMatch(events, id);
        MatchPlayerProjection matchProjection = filteredEventsProjection(match, events, MatchPlayerProjection::new);
        return wrap(matchProjection);
    }

    /**
     * Constructs {@link T match projection} based on supplied match, events and constructor
     * If match is not supplied it returns {@code null}
     *
     * @param match
     * @param events
     * @param constructor
     * @param <T>
     * @return
     */
    private <T extends MatchProjection> T filteredEventsProjection(MatchProjection match, List<EventProjection> events, Function<List<EventProjection>, T> constructor) {
        T matchProjection = null;
        if (match != null) {
            matchProjection = constructor.apply(events);
            mapper.map(match, matchProjection);
        }
        return matchProjection;
    }

    /**
     * Returns {@link MatchProjection match} from supplied events or finds one by {@param #matchJsonId jsonId}
     *
     * @param events
     * @param matchJsonId
     * @return
     */
    private MatchProjection findMatch(Collection<EventProjection> events, int matchJsonId) {
        return events.stream().map(EventProjection::getMatch).findFirst().orElseGet(() -> matchDao.findByJsonId(matchJsonId));
    }

    /**
     * Wrap non-null object in {@link HttpStatus#OK 200 ok} status.
     * Null objects are wrapped in {@link HttpStatus#I_AM_A_TEAPOT 418 I'm a teapot} status.
     *
     * @param t
     * @param <T>
     * @return
     */
    private <T> ResponseEntity<T> wrap(T t) {
        return t != null ? ResponseEntity.ok(t) : ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).build();
    }
}

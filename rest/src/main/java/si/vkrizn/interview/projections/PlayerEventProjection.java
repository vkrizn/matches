package si.vkrizn.interview.projections;

import org.springframework.beans.factory.annotation.Value;

public interface PlayerEventProjection {

    @Value("target.scorer.jsonId")
    Integer getJsonId();

    @Value("#{target.scorer.name} #{target.scorer.surname}")
    String getName();

    @Value("target.scorer.points")
    Integer getPoints();
}

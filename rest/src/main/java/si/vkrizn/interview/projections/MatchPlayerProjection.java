package si.vkrizn.interview.projections;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@Setter
public class MatchPlayerProjection implements MatchProjection {

    private Integer jsonId;
    private TeamProjection home;
    private TeamProjection away;
    private TeamProjection winningTeam;
    private Integer homeResult;
    private Integer awayResult;

    private final List<EventProjection> events;

    public MatchPlayerProjection(List<EventProjection> events) {
        this.events = events;
    }

    @JsonIgnore
    @Override
    public List<EventProjection> getEvents() {
        return events;
    }

    public List<PlayerProjection> getScorers() {
        return events.stream()
                .map(EventProjection::getScorer)
                .filter(Objects::nonNull)
                .distinct()
                .collect(Collectors.toList());
    }
}

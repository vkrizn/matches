package si.vkrizn.interview.projections;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MatchEventsProjection implements MatchProjection {

    private Integer jsonId;
    private TeamProjection home;
    private TeamProjection away;
    private TeamProjection winningTeam;
    private Integer homeResult;
    private Integer awayResult;

    private final List<EventProjection> events;

    public MatchEventsProjection(List<EventProjection> events) {
        this.events = events;
    }
}

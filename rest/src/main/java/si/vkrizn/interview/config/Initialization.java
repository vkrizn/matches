package si.vkrizn.interview.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import si.vkrizn.interview.JsonImportServiceImpl;

@Component
public class Initialization {

    private static final Logger log = LoggerFactory.getLogger(Initialization.class);

    @Value("${matches.file}")
    private String file;

    @Autowired
    private JsonImportServiceImpl service;

    @EventListener
    public void handleInitialImport(ContextRefreshedEvent event) {
        log.info(String.format("Starting with import of file '%s'", file));
        if (service.importJson(file))
            log.info("Import was successful");
        else
            log.warn(String.format("Import of file '%s' failed!", file));
    }
}
